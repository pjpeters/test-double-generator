win32 {
    PYTHON = C:\Python27\python.exe
} else {
    PYTHON = python
}

wrap_gen.input = FILES_TO_WRAP
wrap_gen.output = wrap_${QMAKE_FILE_BASE}.cpp
wrap_gen.commands = $$PYTHON $$system_path(../../testdoublegenerator/generate_mock.py) ${QMAKE_FILE_NAME} ${QMAKE_FILE_OUT}
wrap_gen.variable_out = SOURCES
QMAKE_EXTRA_COMPILERS += wrap_gen
