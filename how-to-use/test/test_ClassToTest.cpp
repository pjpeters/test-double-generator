#include <QtTest>

#include "mock_wrapper.h"

#include "ClassToTest.h"
#include "Dependency.h"

#include "Exceptions.h"

class test_ClassToTest : public QObject {
    Q_OBJECT

public:
    test_ClassToTest();
    ~test_ClassToTest();

private slots:
    void init();
    void cleanup();

    void throws_exception_if_invoked_method_is_not_registered();

    void calls_the_registered_lambda();
    void calls_the_registered_static_function();
    void calls_the_registered_member_function();

    void the_most_recent_extend_wins();

    void emits_signal_with_payload();

private:
    QSharedPointer<ClassToTest> uut;
    QSharedPointer<Mock<Dependency>> mock_Dependency;
};

test_ClassToTest::test_ClassToTest() {
}

test_ClassToTest::~test_ClassToTest() {
}

void test_ClassToTest::init() {
    mock_Dependency = QSharedPointer<Mock<Dependency>>(Mock<Dependency>::createMock());
    uut = QSharedPointer<ClassToTest>(new ClassToTest(mock_Dependency));
}

void test_ClassToTest::cleanup() {
    uut.reset();
    mock_Dependency.reset();
}

void test_ClassToTest::throws_exception_if_invoked_method_is_not_registered() {
    QVERIFY_EXCEPTION_THROWN(
        uut->callDoubleReturningMethodWithIntAndFloatArgument(1, 2), NoFunctionRegistered);
}

void test_ClassToTest::calls_the_registered_lambda() {
    const int intArg = 23;
    const float floatArg = 42.42;
    const double retValExpected = intArg + floatArg;

    mock_Dependency->extend<double, int, float>(
        &Dependency::doubleReturningMethodWithIntAndFloatArgument,
        [](const int firstArg, const float secondArg) -> double {
        return firstArg + secondArg;
    });

    QCOMPARE(uut->callDoubleReturningMethodWithIntAndFloatArgument(intArg, floatArg),
             retValExpected);
}

double staticFunction(const int intArg, const float floatArg) {
    return intArg + floatArg;
}

void test_ClassToTest::calls_the_registered_static_function() {
    const int intArg = 23;
    const float floatArg = 42.42;
    const double retValExpected = intArg + floatArg;

    mock_Dependency->extend(&Dependency::doubleReturningMethodWithIntAndFloatArgument,
                            staticFunction);

    QCOMPARE(uut->callDoubleReturningMethodWithIntAndFloatArgument(intArg, floatArg),
             retValExpected);
}

struct Foo {
    double memberFunction(const int intArg, const float floatArg) {
        return intArg + floatArg;
    }
};

void test_ClassToTest::calls_the_registered_member_function() {
    const int intArg = 23;
    const float floatArg = 42.42;
    const double retValExpected = intArg + floatArg;

    Foo foo;

    mock_Dependency->extend(&Dependency::doubleReturningMethodWithIntAndFloatArgument,
                            foo, &Foo::memberFunction);

    QCOMPARE(uut->callDoubleReturningMethodWithIntAndFloatArgument(intArg, floatArg),
             retValExpected);
}

void test_ClassToTest::the_most_recent_extend_wins() {
    mock_Dependency->extend<double, int, float>(
        &Dependency::doubleReturningMethodWithIntAndFloatArgument,
        [](const int, const float) ->double { return 1; });
    mock_Dependency->extend<double, int, float>(
        &Dependency::doubleReturningMethodWithIntAndFloatArgument,
        [](const int, const float) ->double { return 2; });

    QCOMPARE(uut->callDoubleReturningMethodWithIntAndFloatArgument(0, 0), 2);
}

void test_ClassToTest::emits_signal_with_payload() {
    const int payloadSent = 23;
    QSignalSpy spy(uut.data(), &ClassToTest::signalThatASignalWasReceived);

    emit mock_Dependency->signalWithIntPayload(payloadSent);

    const int payloadReceived = spy.takeFirst().at(0).toInt();
    QCOMPARE(payloadReceived, payloadSent);
}

QTEST_APPLESS_MAIN(test_ClassToTest)

#include "test_ClassToTest.moc"
