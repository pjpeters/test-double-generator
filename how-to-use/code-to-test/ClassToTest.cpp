#include "ClassToTest.h"

ClassToTest::ClassToTest(QSharedPointer<Dependency> dependency)
    : QObject()
    , dependency(dependency) {

    connect(dependency.data(), &Dependency::signalWithIntPayload,
            this, &ClassToTest::signalThatASignalWasReceived);
}

double ClassToTest::callDoubleReturningMethodWithIntAndFloatArgument(const int intArg,
                                                                     const float floatArg) {
    return dependency->doubleReturningMethodWithIntAndFloatArgument(intArg, floatArg);
}
