#ifndef DEPENDENCY_H
#define DEPENDENCY_H

#include <QObject>

class Dependency : public QObject {
    Q_OBJECT;

public:
    virtual double doubleReturningMethodWithIntAndFloatArgument(int intArg, float floatArg) = 0;

signals:
    void signalWithIntPayload(int payload);
};

#endif // DEPENDENCY_H
