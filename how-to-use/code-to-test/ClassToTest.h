#ifndef CLASSTOTEST_H
#define CLASSTOTEST_H

#include <QObject>
#include <QSharedPointer>

#include "Dependency.h"

class ClassToTest : public QObject {
    Q_OBJECT;

public:
    explicit ClassToTest(QSharedPointer<Dependency> dependency);

    double callDoubleReturningMethodWithIntAndFloatArgument(int intArg, float floatArg);

signals:
    void signalThatASignalWasReceived(int payloadReceived);

private:
    QSharedPointer<Dependency> dependency;
};

#endif // CLASSTOTEST_H
